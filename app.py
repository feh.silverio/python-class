# PRINT é comando utilizado para exibir resultados e mensagens na tela.

print("Olá, bem vindo ao curso de Python!")

# INPUT é um comando utilizado para coletar informações do usuário.

input("Qual é o seu nome? ")

# VARIABLES são palavras que armazenam valores

nome = input("Qual o seu nome?")
print(nome)

idade = input("Qual a sua idade?")
print("Sua idade é de:", idade, "anos")



